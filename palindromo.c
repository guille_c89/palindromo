#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

void del_spaces(char *s);
bool is_palin(char *s);
size_t count_pals(char *p);

int main(int argc, char *argv[]){

	if (argc != 2){
		fprintf(stderr, "Invalid number of arguments!\n");
		exit(EXIT_FAILURE);
	}

	printf("Cantidad de palíndromos: %zu\n", count_pals(argv[1]));

	exit(EXIT_SUCCESS);
}

void del_spaces(char *s){
	size_t i = 0;
	size_t j = 0;

	while (*(s + j) != '\0'){
		if (*(s + j) != ' ') *(s + i++) = *(s + j);
		++j;
	}

	*(s + i) = '\0';
}

bool is_palin(char *s){
	size_t l = 0;

	del_spaces(s);

	while (*(s + l + 1) != '\0'){
		++l;
	}

	for (size_t r = 0; r < l; r++, l--){
		if (*(s + r) != *(s + l)){
			return false;
		}
	}

	return true;
}

size_t count_pals(char *p){
	FILE *fp = fopen(p, "r");
	size_t pals = 0;

	if (fp == NULL){
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	else {
		size_t init = 0, len = 0;

		while (!feof(fp) && !ferror(fp)){
			char c = fgetc(fp);

			while (c != '\n' && !feof(fp) && !ferror(fp)){
				len++;
				c = fgetc(fp);
			}

			if (!feof(fp) && !ferror(fp)){
				char *s = malloc(len);

				fseek(fp, init, SEEK_SET);

				for (size_t i = 0; i < len; i++){
					*(s + i) = getc(fp);
				}
				
				*(s + len) = '\0';

				if (is_palin(s)) pals++;

				free(s);
				
				init += len + 1;
				len = 0;

				fseek(fp, init, SEEK_SET);
			}
		}

		if (ferror(fp)){
			perror("ferror");
			exit(EXIT_FAILURE);
		}

		fclose(fp);
	}

	return pals;
}
